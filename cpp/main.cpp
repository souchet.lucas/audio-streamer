#include "audio_streaming.h"
#include <iostream>


int main(int argc, char** argv) {
    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " <address> <mode[string|play-stream]>" << std::endl;
        return 1;
    }
    char* address = argv[1];
    char* mode = argv[2];
    if (strcmp(mode, "string") == 0) {
        void* stream = create_tcp_stream(address, 9001, "cpp");
        char* client_id = read_string_from_stream_c(stream);
        std::cout << "New connection from " << client_id << std::endl;
        free_string(client_id);
        free_tcp_stream(stream);

        return 0;
    } else if (strcmp(mode, "play-stream") == 0) {
        void* stream = create_tcp_stream(address, 9001, "cpp");
        play_wav_stream_c(stream);
        free_tcp_stream(stream);
    } else {
        std::cout << "Usage: " << argv[0] << " <address> <mode[string|play-stream]>" << std::endl;
        return 1;
    }
}
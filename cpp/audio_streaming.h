#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <ostream>
#include <new>

extern "C" {

void play_wav_stream_c(void *stream);

char *read_string_from_stream_c(void *stream);

void free_string(char *s);

void free_tcp_stream(void *stream);

void *create_tcp_stream(const char *address, int32_t port, const char *client_id);

} // extern "C"

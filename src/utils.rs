use chrono::{DateTime, FixedOffset, Utc};
use itertools::Itertools;
use std::cmp::min;
use std::io::{BufReader, Cursor, Read, Write};
use std::net::TcpStream;

use rodio::cpal::traits::{HostTrait, StreamTrait};
use rodio::{cpal, Decoder, DeviceTrait, OutputStream, Sink};

const BUFFER_CHUNK_SIZE: usize = 5;

// Write a string to the stream
fn write_string_to_stream(mut stream: &TcpStream, message: &str) {
    for i in 0..(message.len() / BUFFER_CHUNK_SIZE) {
        let start = i * BUFFER_CHUNK_SIZE;
        let end = (i + 1) * BUFFER_CHUNK_SIZE;
        let chunk = &message[start..end];
        stream.write_all(chunk.as_bytes()).unwrap();
    }
}

// Read a string from the stream
pub fn read_string_from_stream(mut stream: &TcpStream) -> String {
    let mut message = String::new();
    loop {
        let mut buffer = [0; BUFFER_CHUNK_SIZE];
        let size = match stream.read(&mut buffer) {
            Ok(size) => size,
            Err(_) => 0,
        };
        let chunk = String::from_utf8_lossy(&buffer[..size]);
        message = message + chunk.to_string().as_str();
        if size < BUFFER_CHUNK_SIZE {
            break;
        }
    }
    println!("message: {}", message);
    return message;
}

// Struct used to serialize and deserialize dates
struct DateSerializer {}

//TODO Add Unit tests on DateSerializer
impl DateSerializer {
    const FORMAT: &'static str = "%Y%m%dT%H%M%S%.6f%z";
    fn serialize_date(date: &DateTime<Utc>) -> Vec<u8> {
        date.format(Self::FORMAT).to_string().into_bytes()
    }

    fn deserialize_date(bytes: &[u8]) -> Option<DateTime<FixedOffset>> {
        let date = match std::str::from_utf8(bytes) {
            Ok(date) => date,
            Err(_) => "",
        };
        match DateTime::parse_from_str(date, Self::FORMAT) {
            Ok(date) => Some(date),
            Err(_) => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::offset::TimeZone;

    #[test]
    fn test_serialize_date() {
        let date = Utc.ymd(2022, 7, 8).and_hms(9, 10, 11);
        let serialized = DateSerializer::serialize_date(&date);
        let expected = "20220708T091011.000000+0000".as_bytes().to_vec();
        assert_eq!(serialized, expected);
    }

    #[test]
    fn test_deserialize_date() {
        let bytes = "20220708T091011.000000+0000".as_bytes();
        let deserialized = DateSerializer::deserialize_date(bytes).unwrap();
        let expected = Utc.ymd(2022, 7, 8).and_hms(9, 10, 11);
        assert_eq!(deserialized, expected);
    }

    #[test]
    fn test_deserialize_invalid_date() {
        let bytes = "invalid date".as_bytes();
        let deserialized = DateSerializer::deserialize_date(bytes);
        assert!(deserialized.is_none());
    }
    use hound::WavSpec;

    #[test]
    fn test_serialize_wav_spec() {
        let spec = WavSpec {
            channels: 2,
            sample_rate: 44100,
            bits_per_sample: 16,
            sample_format: hound::SampleFormat::Int,
        };
        let serialized = WAVSerializer::serialize_wav_spec(&spec);
        let expected: [u8; WAVSerializer::WAV_HEADER_SIZE] = [2, 0, 68, 172, 0, 0, 0, 16, 0, 0];
        assert_eq!(serialized, expected);
    }

    #[test]
    fn test_deserialize_wav_spec() {
        let bytes: [u8; WAVSerializer::WAV_HEADER_SIZE] = [2, 0, 68, 172, 0, 0, 0, 16, 0, 0];
        let deserialized = WAVSerializer::deserialize_wav_spec(&bytes);
        let expected = WavSpec {
            channels: 2,
            sample_rate: 44100,
            bits_per_sample: 16,
            sample_format: hound::SampleFormat::Int,
        };
        assert_eq!(deserialized, expected);
    }

    #[test]
    fn test_serialize_chunk() {
        let chunk = vec![1, 2, 3, 4];
        let date = Some(Utc.ymd(2022, 7, 8).and_hms(9, 10, 11));
        let serialized = WAVSerializer::serialize_chunk(&chunk, &date);
        let expected = vec![
            8, 0, // size of the chunk
            // serialized date
            50, 48, 50, 50, 48, 55, 48, 56, 84, 48, 57, 49, 48, 49, 49, 46, 48, 48, 48, 48, 48, 48, 43, 48, 48, 48, 48,
            // chunk data
            1, 0, 2, 0, 3, 0, 4, 0,
        ];
        assert_eq!(serialized, expected);
    }
}


struct WAVSerializer {}

//TODO Add Unit tests on WAVSerializer
impl WAVSerializer {
    const CHUNK_HEADER_SIZE: usize = 29;
    const WAV_HEADER_SIZE: usize = 10;

    // Serialize a wavSpec into [u8; 10]
    fn serialize_wav_spec(spec: &hound::WavSpec) -> [u8; Self::WAV_HEADER_SIZE] {
        let mut bytes: [u8; Self::WAV_HEADER_SIZE] = [0; Self::WAV_HEADER_SIZE];
        bytes[0..2].clone_from_slice(&spec.channels.to_le_bytes());
        bytes[2..6].clone_from_slice(&spec.sample_rate.to_le_bytes());
        bytes[7..9].clone_from_slice(&spec.bits_per_sample.to_le_bytes());
        bytes[9] = match spec.sample_format {
            hound::SampleFormat::Int => 0,
            hound::SampleFormat::Float => 1,
        };
        println!("Sending spec: {:?}", bytes);
        bytes
    }

    fn deserialize_wav_spec(bytes: &[u8]) -> hound::WavSpec {
        hound::WavSpec {
            channels: u16::from_le_bytes(bytes[0..2].try_into().unwrap()),
            sample_rate: u32::from_le_bytes(bytes[2..6].try_into().unwrap()),
            bits_per_sample: u16::from_le_bytes(bytes[7..9].try_into().unwrap()),
            sample_format: match bytes[9] {
                0 => hound::SampleFormat::Int,
                _ => hound::SampleFormat::Float,
            },
        }
    }

    fn serialize_chunk(chunk: &Vec<i16>, date: &Option<DateTime<Utc>>) -> Vec<u8> {
        // Prepare the chunk header
        let header = vec![
            (2 * chunk.len() as u16).to_le_bytes().to_vec(),
            match date {
                Some(date) => DateSerializer::serialize_date(&date),
                None => vec![0; 27],
            },
        ];
        let buffer = header.into_iter().flatten().collect::<Vec<u8>>();
        assert_eq!(buffer.len(), Self::CHUNK_HEADER_SIZE);
        // convert chunk from Vec<i16> to [u8]
        chunk.iter().fold(buffer, |mut acc, x| {
            acc.append(&mut x.to_le_bytes().to_vec());
            acc
        })
    }

    fn deserialize_chunk(bytes: &[u8]) -> (u16, Option<DateTime<FixedOffset>>) {
        let data_size = u16::from_le_bytes(bytes[..2].try_into().unwrap());
        let timestamp = DateSerializer::deserialize_date(&bytes[2..]);
        (data_size, timestamp)
    }
}


// Send a WAV header to the stream
fn send_wav_spec(mut stream: &TcpStream, spec: &hound::WavSpec) {
    stream
        .write_all(&WAVSerializer::serialize_wav_spec(spec))
        .unwrap();
}

// Write a full i16 wav buffer to the stream
fn write_audio_chunk(mut stream: &TcpStream, chunk: &Vec<i16>, date: Option<DateTime<Utc>>) {
    // Send the chunk into the stream
    stream
        .write_all(&WAVSerializer::serialize_chunk(chunk, &date))
        .unwrap();
    stream.flush().unwrap();
}

// Read a full i16 wav data buffer from the stream
fn read_audio_samples_u16(mut stream: &TcpStream) -> Vec<i16> {
    let mut result = Vec::<i16>::new();
    let mut header: [u8; WAVSerializer::CHUNK_HEADER_SIZE] = [0; WAVSerializer::CHUNK_HEADER_SIZE];
    loop {
        let mut data_size = 0;
        let header_size = match stream.read(&mut header) {
            Ok(size) => {
                if size > 0 {
                    (data_size, _) = WAVSerializer::deserialize_chunk(&header);
                }
                size
            }
            Err(_) => 0,
        };
        if header_size == 0 {
            return result;
        }
        println!("Ready to read {} bytes", data_size);
        // Reading data_size bytes through CHUNK_SIZE chunks
        const CHUNK_SIZE: usize = 1024;
        while data_size > 0 {
            let mut chunk = vec![0; min(data_size as usize, CHUNK_SIZE)];
            let size = match stream.read(&mut chunk) {
                Ok(size) => size,
                Err(_) => 0,
            };
            if size == 0 {
                break;
            }
            for j in 0..size / 2 {
                let sample = i16::from_le_bytes([chunk[2 * j], chunk[2 * j + 1]]);
                result.push(sample);
            }
            data_size -= size as u16;
        }
    }
}

// Receive a WAV header from the stream
fn receive_wav_spec(mut stream: &TcpStream) -> hound::WavSpec {
    let mut bytes = [0; 10];
    stream.read_exact(&mut bytes).unwrap();
    WAVSerializer::deserialize_wav_spec(&bytes)
}

// Receive a complete WAV buffer from the stream
pub fn receive_raw_audio(stream: &TcpStream) -> (hound::WavSpec, Vec<i16>) {
    let header = receive_wav_spec(&stream);
    //TODO Read samples according to spec
    let samples_i16 = read_audio_samples_u16(&stream);
    println!("Received {} samples", samples_i16.len());
    (header, samples_i16)
}

// Save the WAV buffer to a file
pub fn save_wav(spec: &hound::WavSpec, samples_i16: &Vec<i16>, path: &str) {
    println!("Spec is {:?}", spec);
    let mut writer = hound::WavWriter::create(path, *spec).unwrap();
    samples_i16.iter().for_each(|x| {
        writer.write_sample(*x).unwrap();
    });
    writer.finalize().unwrap();
}

// Plays the whole wav buffer on the defoult output device
pub fn play_full_wav(spec: &hound::WavSpec, samples_i16: &Vec<i16>) {
    let mut buffer = Cursor::new(Vec::new());
    {
        let mut writer = hound::WavWriter::new(&mut buffer, *spec).unwrap();
        samples_i16.iter().for_each(|x| {
            writer.write_sample(*x).unwrap();
        });
        writer.finalize().unwrap();
    }
    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let reader = BufReader::new(Cursor::new(buffer.into_inner()));
    let source = Decoder::new_wav(reader).unwrap();
    let sink = Sink::try_new(&stream_handle).unwrap();
    sink.append(source);
    sink.sleep_until_end();
}

fn write_stream<T: std::io::Write + std::io::Seek>(
    mut stream: &TcpStream,
    writer: &mut hound::WavWriter<T>,
) -> Option<DateTime<FixedOffset>> {
    let mut timestamp: Option<DateTime<FixedOffset>> = None;
    let mut header: [u8; WAVSerializer::CHUNK_HEADER_SIZE] = [0; WAVSerializer::CHUNK_HEADER_SIZE];
    let mut data_size = 0;
    let header_size = match stream.read(&mut header) {
        Ok(size) => {
            if size > 0 {
                (data_size, timestamp) = WAVSerializer::deserialize_chunk(&header);
            }
            size
        }
        Err(_) => 0,
    };
    if header_size == 0 {
        return timestamp;
    }
    println!("Ready to read {} bytes", data_size);
    // Reading data_size bytes through CHUNK_SIZE chunks
    const CHUNK_SIZE: usize = 1024;
    while data_size > 0 {
        let mut chunk = vec![0; min(data_size as usize, CHUNK_SIZE)];
        let size = match stream.read(&mut chunk) {
            Ok(size) => size,
            Err(_) => 0,
        };
        if size == 0 {
            break;
        }
        wav_write(&chunk, writer, size);
        data_size -= size as u16;
    }
    return timestamp;
}

// Saves the wav buffer as it is received from the server
pub fn save_wav_stream(path: &str, mut stream: &TcpStream) {
    let spec = receive_wav_spec(&stream);
    let mut writer = hound::WavWriter::create(path, spec).unwrap();
    loop {
        match write_stream(stream, &mut writer) {
            None => break,
            _ => (),
        }
    }
    writer.finalize().unwrap();
}

// Write the wav buffer to the writer
fn wav_write<T: std::io::Seek + std::io::Write>(
    bytes: &[u8],
    writer: &mut hound::WavWriter<T>,
    size: usize,
) {
    for j in 0..size / 2 {
        let sample = i16::from_le_bytes([bytes[2 * j], bytes[2 * j + 1]]);
        writer.write_sample(sample).unwrap();
    }
}

// Plays the wav buffer as it is received from the server
// TODO Use read_chunk
pub fn play_wav_stream(mut stream: &TcpStream) {
    let mut i = 0;
    let spec = receive_wav_spec(&stream);
    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let sink = Sink::try_new(&stream_handle).unwrap();
    println!("Playing wav stream");
    loop {
        let mut buffer = Cursor::new(Vec::new());
        let mut timestamp = None;
        {
            let mut writer = hound::WavWriter::new(&mut buffer, spec).unwrap();
            timestamp = write_stream(stream, &mut writer);
        }

        let reader = BufReader::new(Cursor::new(buffer.into_inner()));
        let source = Decoder::new_wav(reader).unwrap();
        if !timestamp.is_none() {
            let now = Utc::now();
            let duration = timestamp.unwrap().signed_duration_since(now);
            println!("duration is {:?}", duration);
            if timestamp.unwrap() > now {
                // Converting chrono::Duration to std::time::Duration, taking into accoung that
                // chrono::Duration::num_nanoseconds() returns the number of nanoseconds including the number of seconds
                let std_duration = std::time::Duration::new(
                    duration.num_seconds() as u64,
                    (duration.num_nanoseconds().unwrap() - duration.num_seconds() * 1000000000)
                        as u32,
                );

                println!("Timestamp is in the future, waiting for {:?}  ", std_duration);
                std::thread::sleep(std_duration);
            } else if timestamp.unwrap() < now - chrono::Duration::milliseconds(100) {
                println!("Timestamp is in the past, skipping");
                continue;
            }
        }
        sink.append(source);
        i += 1;
    }
}

// Send the microphone stream to the stream
pub fn send_microphone_stream(stream: TcpStream) {
    let host = cpal::default_host();
    let device = host
        .default_input_device()
        .expect("Failed to get default input device");
    let format = cpal::traits::DeviceTrait::default_input_config(&device)
        .expect("Failed to get default input format");

    // Create the WAV writer.
    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: format.sample_rate().0 as u32,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };
    // Send the WAV header
    send_wav_spec(&stream, &spec);
    //TODO Move this into a function
    let input_stream = device
        .build_input_stream(
            &format.config(),
            move |data: &[f32], _: &cpal::InputCallbackInfo| {
                println!("Received {} samples", data.len());
                data.chunks(1024).into_iter().for_each(|x| {
                    let chunk_i16 = x.iter().fold(Vec::<i16>::new(), |mut acc, y| {
                        acc.push((y * i16::MAX as f32) as i16);
                        acc
                    });
                    write_audio_chunk(&stream, &chunk_i16, None);
                });
            },
            |err| {
                eprintln!("An error occurred on stream: {}", err);
            },
            None,
        )
        .expect("Failed to build input stream");
    input_stream.play().expect("Failed to play stream");
    loop {}
}

pub fn send_sample_str(stream: &TcpStream) {
    const BUFFER_SIZE: usize = 20;
    let mut message: String = String::new();
    for i in 0..BUFFER_SIZE {
        message = message + String::from((i as u8 + b'a') as char).as_str();
    }
    println!("Sending message: {}", message);
    write_string_to_stream(&stream, &message);
}

// Send a audio wav file to the stream
pub fn send_raw_audio(mut stream: &TcpStream, path: &str, start_date: &DateTime<Utc>) {
    let mut reader = hound::WavReader::open(path).unwrap();
    send_wav_spec(&stream, &reader.spec());
    // Chunk size has to be a multiple of bit per sample value
    const CHUNK_SIZE: usize = 1024;
    let mut i = 0;
    let sample_rate = reader.spec().sample_rate;
    reader
        .samples::<i16>()
        .chunks(CHUNK_SIZE)
        .into_iter()
        .enumerate()
        .for_each(|x| {
            let chunk = x.1.collect::<Result<Vec<i16>, _>>().unwrap();
            // Compute the timestamp
            let sample_date = *start_date
                + chrono::Duration::milliseconds(
                    (1000 * x.0 * CHUNK_SIZE) as i64 / sample_rate as i64,
                );
            if sample_date > Utc::now() {
                // Do not send the chunk if the timestamp is in the past
                write_audio_chunk(&stream, &chunk, Some(sample_date));
                i += 1;
            }
        });
    println!("Finished sending {} samples in {} steps", reader.len(), i);
    stream.flush().unwrap();
}

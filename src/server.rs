mod utils;
use crate::utils::{
    read_string_from_stream, send_microphone_stream, send_raw_audio, send_sample_str,
};
use chrono::{Utc, DateTime};
use clap::Parser;
use std::net::TcpListener;

fn start_server(args: &Args) {
    let listener = match TcpListener::bind(String::from(&args.ip) + ":9001") {
        Ok(listener) => listener,
        Err(e) => {
            println!("Failed to bind server to port 9001: {}", e);
            return;
        }
    };
    let mut start_date: Option<DateTime<Utc>> = None;
    let mut end_date: Option<DateTime<Utc>> = None;
    for stream in listener.incoming() {
        let tcp_stream = stream.unwrap();
        let client_id = read_string_from_stream(&tcp_stream);
        println!("New connection from {}", client_id);
        if args.microphone_in {
            std::thread::spawn(move || {
                send_microphone_stream(tcp_stream.try_clone().unwrap());
            });
        } else if args.string {
            std::thread::spawn(move || {
                send_sample_str(&tcp_stream.try_clone().unwrap());
            });
        } else if args.wav_file {
            let path = String::from("src/assets/CantinaBand60.wav");
            // We hard code the duration here, but we could also read it from the file.
            let wav_duration_seconds = 60;
            if start_date.is_none() || end_date.is_none() || Utc::now() > end_date.unwrap() {
                println!("Resetting start date");
                start_date = Some(Utc::now() + chrono::Duration::seconds(5));
                end_date = Some(start_date.unwrap() + chrono::Duration::seconds(wav_duration_seconds));
            }
            std::thread::spawn(move || {
                send_raw_audio(&tcp_stream.try_clone().unwrap(), path.as_str(), &start_date.unwrap());
            });
        }
    }
}

/// Audio streaming server
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long, default_value = "127.0.0.1")]
    ip: String,

    #[arg(short, long)]
    microphone_in: bool,

    #[arg(short, long)]
    wav_file: bool,

    #[arg(short, long)]
    string: bool,
}

fn main() {
    let args = Args::parse();
    start_server(&args);
}

use std::{net::TcpStream, ffi::{c_char, CString, CStr}, ptr, io::Write};

use crate::utils::{play_wav_stream, read_string_from_stream};


#[no_mangle]
pub unsafe extern "C" fn play_wav_stream_c(stream: *mut TcpStream) {
    let stream = match stream.as_mut() {
        Some(stream) => stream,
        None => return,
    };
    play_wav_stream(&stream);
}

#[no_mangle]
pub unsafe extern "C" fn read_string_from_stream_c(stream: *mut TcpStream) -> *mut c_char {
    let stream = match stream.as_mut() {
        Some(stream) => stream,
        None => return ptr::null_mut(),
    };
    let message = read_string_from_stream(&stream);
    let c_string = CString::new(message).unwrap();
    return c_string.into_raw();
}

#[no_mangle]
pub unsafe extern "C" fn free_string(s: *mut c_char) {
    if s.is_null() {
        return;
    }
    drop(CString::from_raw(s));
}

#[no_mangle]
pub unsafe extern "C" fn free_tcp_stream(stream: *mut TcpStream) {
    if stream.is_null() {
        return;
    }
    drop(Box::from_raw(stream));
}

#[no_mangle]
pub unsafe extern "C" fn create_tcp_stream(
    address: *const c_char,
    port: i32,
    client_id: *const c_char,
) -> *mut TcpStream {
    let address = CStr::from_ptr(address).to_str().unwrap();
    let client_id = CStr::from_ptr(client_id).to_str().unwrap();

    let mut tcp_stream =
        TcpStream::connect(String::from(address) + ":" + &port.to_string()).unwrap();
    tcp_stream.write_all(client_id.as_bytes()).unwrap();
    tcp_stream.flush().unwrap();
    return Box::into_raw(Box::new(tcp_stream));
}
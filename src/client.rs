mod utils;
use clap::Parser;
use std::{
    net::TcpStream,
    thread, io::Write,
};
use utils::{read_string_from_stream, play_wav_stream, save_wav_stream, receive_raw_audio, play_full_wav, save_wav};

fn start_client(args: &Args, client_id: &str) {
    let mut tcp_stream = match TcpStream::connect(String::from(&args.ip) + ":9001") {
        Ok(tcp_stream) => tcp_stream,
        Err(e) => {
            println!("Failed to connect to server at {}: {}", &args.ip, e);
            return;
        }
    };
    tcp_stream.write_all(client_id.as_bytes()).unwrap();
    tcp_stream.flush().unwrap();
    println!("Hello from {}", client_id);
    let path = String::from("/tmp/received_") + client_id + ".wav";
    if args.stream {
        if args.play {
            play_wav_stream(&tcp_stream);
        }
        if args.save {
            save_wav_stream(&path, &tcp_stream);
        }
    } else if args.full {
        let samples = receive_raw_audio(&tcp_stream);
        if args.play {
            play_full_wav(&samples.0, &samples.1);
        }
        if args.save {
            save_wav(&samples.0, &samples.1, path.as_str());
        }
    } else if args.string {
        let message = read_string_from_stream(&tcp_stream);
        println!("Received message: {}", message);
    }
}



/// Audio streaming client
#[derive(Parser, Debug, Clone)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long, default_value = "127.0.0.1")]
    ip: String,

    #[arg(long)]
    string: bool,

    #[arg(long)]
    stream: bool,

    #[arg(long)]
    full: bool,

    #[arg(long)]
    play: bool,

    #[arg(long)]
    save: bool,

    #[arg(short, long, default_value = "1")]
    count: usize,
}

fn main() {
    let args = Args::parse();
    let mut handles = vec![];
    for i in 0..args.count {
        println!("Starting client {}", i);
        let client_id = String::from("client ") + i.to_string().as_str();
        let arg = Args::parse();
        let handle = thread::spawn(move || {
            start_client(&arg, &client_id.clone());
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }
}

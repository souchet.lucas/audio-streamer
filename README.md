# Audio Streaming service

## Requirements
This project has been only built and tested on MacOS 14.0 on a Macbook Pro M1 Pro.

Besides the installation of rust on the host, it also requires to install the lib portaudio. This is done on MacOS with the command `brew install portaudio`.

The Android client has been built using Android Studio and tested with Android 14 on a Pixel 7 device.

The C++ client requires cmake > 3.5.10 and has been tested with version 3.27.9 and clang 14.0.3.

## Getting started

Start the client and the server in separate command lines
```shell
cargo run --bin server -- --wav-file
```

```shell
cargo run --bin client -- --stream --play
```

When the client connects to the server, it will advertise itself to the server

```shell
message: Client A
New connection from Client A
```

In response, the server returns the wav stream and the client will play it after 5 seconds on the host speakers.

## Demo
A demo video is available [here](https://gitlab.com/souchet.lucas/audio-streamer/-/wikis/Video-demo)

## Command line

Several modes are supported for the server and the server:

| Server | Client | Description |
|---|---|---|
| `--string` | `--string`| When receiving a new connection, the server will send a suite of char that will be displayed by the client |
| `--wav-file`|  | When receiving a new connection, the server will send a wav file buffer (according to the specified protocol) timestamped to be played after a 5s delay. All client connecting during this broadcast will play the same stream synchronously |
| `--microphone-in`|  | When receiving a new connection, the server will send a the wav stream of the host microphone|
| | `--full` | When receiving data, the client will get all the buffer before processing it (playing on speakers or saving to file) |
| | `--stream` | When receiving data, the client will processing as it is received (playing it synchronously on speakers or saving to file) |
| | `--play` | When enabled the client will play the received stream on the host speakers |
| | `--save` | When enabled the client will save the stream into a file (in /tmp/received.wav) |


## Use cases

Based on the given tasks, I have identified different use cases:
1. UC1: Broadcast a microphone stream live to multiple clients.
1. UC2: Broadcast a music stream synchronously on several speaker devices (for surround-like installation)

In UC1, all clients should be able to retrieve the stream and process it right away.

In UC2, the server needs to add metadata for the samples to be played synchronously by all the clients.

## Communication Protocol

The communication protocol is a custom one. Several standard audio streaming protocol are released for various needs, but I assumed it made more sense for this project to implement it myself, in order to identify the constraints and pitfalls (sometimes falling in them :D)

### WAV Header
When sending a stream, the server first send the WAVHeader buffer

It consist in 9 bytes: 

| data | length |
|---|---|
| channels number | (MONO, STEREO, ...) as 2 bytes |
| sample rate | (22kHz, 44.1kHz, ...) as 4 bytes |
| bits per sample | (16, 24, 32, ...) as 2 bytes |
| sample format | (Int, Float) as 1 byte|

After that the server sends the data buffer. The buffer is split into chunks of 1024 samples that consist in two parts

### Sample header
First a sample header is sent of 29 bytes:
|data| length|
|--|--|
|chunk size | integer as 2 bytes|
|timestamp | string as 27 bytes |

### Data payload
The data payload is a buffer of the number of elements specified in the header and contain the sample value according to the bits per sample param
|data|length|
|--|--|
|sample | value over 2 bytes|

### Next: Protocol improvements
The current implementation is very inefficient. A lot of improvements should be made to reduce its size:
- Reduce the size of the WAVHeader by supporting only enum instead of actual values
- Embed the chunk size in the WAVHeader and send fixed sized chunks
- Add a `STREAMING` mode bit to avoid sending a timestamp when it is not needed by the client (live processing)
- Reduce the size of the timestamp field by sending the epoch value rather than an CharArray

## Steps

[X] 1. Init the project with server and client bins

[X] 2. Setup communication between the two and Send string between server and client

[X] 3. Use Hound lib to load a wav and send it to the client that will save it

[X] 4. Client to play the wav to the speaker/headset

~~[] Client to play the sound to a headset~~ Same as above

~~[] Communicate through standard protocol (webRTC?)~~ (probably useful for a maintained project)

[X] 5. Server to get the input from the microphone, client playing it on the headset

~~[] Larsen compensation?~~ (Mostly signal processing, out of scope)

[X] 6. Stream synchronization between multiple clients

[X] 7. Android client to play the sound on the android device speaker

[X] 8. C++ bindings to write a c++ client

~~[] Network discovery and add acknowledgment~~ (may be useful as an evolution of UC2)

[] 9. Encryption and compression

## Details

### 1. Init the project with server and client bins
----------------------------
Create a rust project with two binaries: one for the server and one for the client.

### 2. Setup communication between the two
---
- Lookup basic TCP communication sample from https://linuxhint.com/rust-simple-socket-example/
- Reformat messages to test out the message buffer chunks size
- Extract the read function into a separate utils file
- Implement very basic protocol

### 3. Use hound lib to send a wav file
- The wav is loaded with a hound's WavReader and the i16 samples are split into u8 arrays (With `le_bytes()`) before being sent in the socket
- The WavSpec of the WavReader needs to be shared from the server to the client to allow him to parse the buffer correctly. It is sent first and the data (sample rate, bit per sample, ...) are to be used when deserializing the buffer.
- The approach of writing individual samples into the buffer is probably inefficient when using remote calls as TCP headers will represent an overhead. I grouped them into chunks before sending them. The size of the chunks should take into account the bit per sample to prevent samples to be partially sent.
- TODO: refactor the code to expose different backends to easily group interfaces for reading and writing procedures


### 4. Play the WAV on the host speakers
- The wav is loaded as above and feeded to a Sink which is directed to the speakers outputStream. The lib CPAL is used to interact with the audio devices. The crate is pulled as a dependency of Hound, but it requires to work the installation of portaudio on the host.

On MacOS, `brew install portaudio` 

I added a sink to play the WAV buffer as it was received from the server. Note: Reading the sample one by one was quite expensive, and resulted in blocking the socket, so the write from the server were longer.
I read the buffer by chunks to reduce the number of read operations. The cause might be related to the interruption caused by syscalls (as I am testing in local network). For WAN, one should pay attention to the size of the packets to avoid Jumbos (large ethernet packets that might not be handled properly by routing equipments and cause congestion)
- TODO: Right now, the client receives the WAV header but only deserializes 16bits streams. The support of other types is fairly easy and it only concerns the parsing of the samples.

### 5. Send microphone input stream
CPAL is used to retrieve the host microphone input stream. This is a good opportunity to experience larsen effect when playing the microphone streams on the same host's speakers (watch your ears :D ).

The stream is provided as a wav of 16 bits per sample, and 16kHz in Mono

### 6. Synchronization
To support UC2, one key point is to make sure that multiple clients are able to play the same stream synchronously. For example, two connected speakers could be used to play in stereo a stream synchronously.

It is implemented:
- In the server by adding an expected play timestamp in the header of each sample
- In the client by ignoring old samples, and sleep when receiving a sample in the future to add the sample to the sink only at the time specified by the timestamp

This naive approach is good enough, but the precision of the playback is not optimal as milliseconds of delay between two clients can be heavily impacting the user experience. Improvement would require deeper investigation of the latency chain between the add of the source to the sink to the time the sound is actually played on the speakers.

Also, the communication protocol can add an overhead as every sample are timestamped.

An improvement of the protocol could be to specify only the starting date of the whole stream, or of bigger chunks

#### Clock synchronization
Another big limitation of the current implementation is that it relies on each client's clock to ensure the samples are played at the right time. This is a wrong estimate in general. 
Several protocol exist to synchronize clocks, such as NTP (by connecting to a server and exchanging messages to synchronize the clock), or PTP. 
We should implement one of thos to ensure a good playback synchronization.

### Multiple Clients
When using multiple clients, the sever will send the whole buffer every time a new client connects. As a result, it will sequentially wait for each transmission to be over before handling the next client. This can be problematic for live audio as it never really end.

To answer this problem, the server runs the communication with different client in a separate threads (parallel) or with async communication.

Managing dedicated threads can be expensive in terms of resources, but we could define a thread pool with a limited max number of threads. Additional clients would be blocked waiting for available seats..

### 7. Android client 
For the Android client, I created a sample application and used the Android Socket to connect to the Server.
I implemented manually the communication protocol to read the custom wav header, the samples headers with data for synchronization and the associated data.
I played the stream through AudioPlayer.
The Android client is equivalent to the rust client with option `--stream` and `--play`.

The Client is located in the `AndroidAudioClient` subfolder and should be installed and run through Android Studio.

### 8. C++ client
The c++ client is in the `cpp` subfolder. It contains:
- a `CMakeFiles.txt` that configures the cpp build and locate the library and header files
- a `main.cpp` for the client to retrieve a string of play the received stream. Those are the only two modes implemented.
- a `audio_streaming.h` that contains the header of the c functions exposed by the shared library. it as been generated with cbindgen, but manually cleaned

The CmakeFiles looks for the .so library in the rust project release target directory. This requires to change the crate-type to `cdylib` in the `Cargo.toml`. This has a weird side effect of changing the visibility of libs from each other. 
I didn't have time to solve this but this prevented me to split the functions further into separate files.

The header is generated with cbindgen, but at the moment, the TcpStream is included in the signatures and has to be manually changed to void* to be understood by the c library.

```
bindgen . -o target/release/audio_streaming.h
```

The binary is built with
```
cargo build --release
cd cpp
mkdir build
cd build
cmake ..
make
./AudioClient 127.0.0.1 play-stream
```

### Other stacks

To use C++ or Kotlin clients there are different approaches.
1. Implement native applcation (c++ or Kotlin) that implement the same communication protocol when listening on the specified socket

(-) Would need to be manually fixed when changing the protocol

(+) The native data structure can be used directly

This is how the Android client is implemented.

2. Generate bindings for the rust library containing the functions used by the client

(+) When changing the protocol, we can update the lib to ease the adaptation on client side (only requires lib installation)

(-) In this case, wrappers function are bound to expose in C++ the objects and functions, but these functions are unsafe and cannot take advantage of the type and memory safety of Rust.

This is how the C++ client is implemented

## Open Questions

### TCP vs UPD?

TCP provides ordering of the packets and retry strategy in case of a failure, but it comes at the cost of an overall overhead.
UPD is more suitable for data streaming, but then the client has to deal with packet reordering, failure redundancy.
Open question: It might be sufficient for audio streaming but more problematic for conversation where the stream has a lower quality and a cut is more problematic?

Different standard protocol for data streaming seem to exist using TCP and UDP, so we would need to review them in details.

## Known issues (What's next?)
### Various fixes (with T shirt estimates)
(S)- Remove all `unwrap()` that can cause a Panic in corner cases

(M)- Better communication error handling. When the client starts without the server being up, when the client disconnects abruptly or when anything unexpected happens

(M) - Factorization of utils code into separate classes and improvement of code coverage. There seems to be a difference in the modules visibility when using cdylib crate-type.

(L) - Deep dive on the synchronization mechanism to ensure millisecond synchronization. Maybe standard protocol already support that

(L) - Expose a JNI interface for Android app. The model of JNI bindings seems to be targetting a specific application package name and comes with stronger restriction on the build types, so it seems very different from CPP bindings.

### CI
We should add CI jobs to build and test the binaries when commits are added. Gitlab-CI would be pretty straight forward to use in the case. 

Two stages could be used to make sure the build succeeds and all the tests pass.

This is useful to identifiy ASAP potential regressions when implementing new features, but also to ease to update of dependencies. A nightly job could update all dependencies and make sure the project still compiles and the tests still pass.

### Deployment
For the UC1 and UC2, it would make sense for the server to be hosted online. In this case (as well as for development workflow), it should be packaged as a docker image containing the binaries and libraries ready to use. Deploying this kind of docker image is fairly simple using serverless architecture or kubernetes container orchestration.

In all cases, it should be associated with a web server framework, Rust provide a few, such as Actix or Rocket.

### Web security
The current implementation does not include any kind of security. When moving forward, we should implement:
1. encryption through various mechanisms relying on asymmetric cryptography to ensure the data exchanged between the server and the client is only accessible to them
1. authentication of the client from the server to make sure only identified clients can receive the data stream, wether it is for privacy or licensing
1. Authentication of the server from the client so that not malicious user can act as the server and send dangerous data to the client

All 3 above could be adressed with the implementation of mTLS authentication which rely on the exchange of cryptographic secrets based on asymmetric key pairs of both parties.

The authentification itself could be done by checking that the certificate of the other peer is signed by a trusted authority or that it is explicitely white listed (certificate pinning).

### Performance / Compression
As stated above, the implementation of the protocol is pretty naive and no assessment of the performances where made. 

For media transfer, the latency is crucial and its reduction is a matter of tradeoff between computational power and network bandwidth. Reducing the data size (through compression) will increase the data transfer, but at the cost of requiring processing of the received stream. On the other hands some compression or data processing techniques can be used to improve the overall performance, for example taking advantage of DSP or GPUs.
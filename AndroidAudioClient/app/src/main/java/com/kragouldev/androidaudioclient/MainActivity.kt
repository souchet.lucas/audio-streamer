package com.kragouldev.androidaudioclient

import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioTrack
import android.net.DhcpInfo
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import com.kragouldev.androidaudioclient.ui.theme.AndroidAudioClientTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.Socket
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.time.Duration
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


private const val REQUEST_EXTERNAL_STORAGE = 1
private val PERMISSIONS_STORAGE = arrayOf<String>(
    "Manifest.permission.READ_EXTERNAL_STORAGE",
    "Manifest.permission.WRITE_EXTERNAL_STORAGE"
)
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AndroidAudioClientTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyApp()
                }
            }
        }

    }
}

@Composable
fun MyApp() {
    var isConnected by remember { mutableStateOf(false) }
    var wavHeader by remember { mutableStateOf<WavHeader?>(null) }

    LaunchedEffect(Unit) {
        withContext(Dispatchers.IO) {
            println("Starting client")
            val socket = Socket("127.0.0.1", 9001)
            socket.getOutputStream().write("Hello from Android".toByteArray())
            isConnected = true
            wavHeader = readWavHeader(socket)
            startClient(socket, wavHeader)
        }
    }

    DisplayScreen(isConnected, wavHeader)
}

data class WavHeader(val channels: Short, val sampleRate: Int, val bps: Short, val format:Byte)
fun readWavHeader(socket: Socket): WavHeader {
    var wavSpecByteArray = ByteArray(10)
    var buf = ByteBuffer.allocate(10).order(ByteOrder.LITTLE_ENDIAN)
    socket.getInputStream().read(wavSpecByteArray, 0, 10)
    buf.put(wavSpecByteArray)
    buf.rewind()
    val channels = buf.short
    val sampleRate = buf.int
    val padding = buf.get()
    val bps = buf.short
    val format = buf.get()
    println("channels $channels")
    println("sampleRate $sampleRate")
    println("bps $bps")
    println("format $format")
    return WavHeader(channels, sampleRate, bps, format)
}

data class WavSampleHeader(val chunkSize: Short, val timestamp: ZonedDateTime?)
fun readSampleHeader(socket: Socket): WavSampleHeader? {
    val chunkHeaderBuffer = ByteArray(29)
    if (socket.getInputStream().read(chunkHeaderBuffer) == -1) {
        println("Unable to read the chunk header");
        return null
    }
    var headerBuffer = ByteBuffer.allocate(29).order(ByteOrder.LITTLE_ENDIAN)
    headerBuffer.put(chunkHeaderBuffer)
    headerBuffer.rewind()
    val chunkSize = headerBuffer.short
    var dateStr = ""
    for (i in 0..26) {
        dateStr += headerBuffer.get().toInt().toChar()
    }
    val dateFormatStr = "yyyyMMdd'T'HHmmss.SSSSSSX"
    val formatter = DateTimeFormatter.ofPattern(dateFormatStr)
    var date: ZonedDateTime? = null
    try {
        date = ZonedDateTime.parse(dateStr, formatter)
    } catch (e: Exception) {
        println("date parsing error $e")
    }
    return WavSampleHeader(chunkSize, date)
}

fun startClient(socket: Socket, wavHeader: WavHeader?) {



    // Set up AudioTrack
    val channelMask = when (wavHeader?.channels) {
        1.toShort() -> AudioFormat.CHANNEL_OUT_MONO
        else -> AudioFormat.CHANNEL_OUT_STEREO
    }
    val encoding = when (wavHeader?.bps) {
        16.toShort() -> AudioFormat.ENCODING_PCM_16BIT
        24.toShort() -> AudioFormat.ENCODING_PCM_24BIT_PACKED
        32.toShort() -> AudioFormat.ENCODING_PCM_32BIT
        else -> {
            println("Unknown encoding ${wavHeader?.bps}, falling back to 16 bits")
            AudioFormat.ENCODING_PCM_16BIT}
    }

    val minBufferSize = AudioTrack.getMinBufferSize(wavHeader?.sampleRate!!.toInt(), channelMask, encoding)
    println("Received wav header: $wavHeader")
    //TODO: Display "connected" button
    //TODO: Display delay

    val audioTrack = AudioTrack.Builder()
        .setAudioAttributes(
            AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )
        .setAudioFormat(
            AudioFormat.Builder()
                .setEncoding(encoding)
                .setSampleRate(wavHeader!!.sampleRate)
                .setChannelMask(channelMask)
                .build()
        )
        .setBufferSizeInBytes(minBufferSize)
        .build()
    audioTrack.play()

    // Start streaming the WAV data in a separate thread
    Thread {
        try {

            // Read chunk header
            var finished = false
            while (!finished) {
                val chunkHeader = readSampleHeader(socket)
                if (chunkHeader == null) {
                    finished = true
                    continue
                }
                // Read data buffer
                val buffer = ByteArray(chunkHeader.chunkSize.toInt())
                var bytesRead: Int
                socket.getInputStream().read(buffer).also { bytesRead = it }
                if (chunkHeader.timestamp != null) {
                    val now = ZonedDateTime.now(chunkHeader.timestamp?.zone);
                    val duration = Duration.between(now, chunkHeader.timestamp);
                if (duration != null && chunkHeader.timestamp?.compareTo(now)!! > 0 ) {
                    println("Sleeping for ${duration.toMillis()}ms")
                    Thread.sleep(duration.toMillis())
                    audioTrack.write(buffer, 0, bytesRead)
                }
                } else  {
                    audioTrack.write(buffer, 0, bytesRead)
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            println("End of thread")
            // Close the stream and release the AudioTrack when done
            audioTrack.release()
            socket.close()
        }
    }.start()


}

@Composable
fun DisplayScreen(isConnected: Boolean, wavHeader: WavHeader?) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Display live updates based on the connection status
        Text(if (isConnected) "Connected" else "Connecting...", fontSize = 24.sp)
        if (isConnected && wavHeader != null) {
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = "WAV Header: ${wavHeader.toString()}",
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewApp() {
    AndroidAudioClientTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            MyApp()
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AndroidAudioClientTheme {
        Greeting("Android")
    }
}